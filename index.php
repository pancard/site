<?php
    require_once('scripts/xajax/xajax_core/xajax.inc.php');
    $xajax = new xajax(); //On initialise l'objet xajax.
    $xajax->register(XAJAX_FUNCTION, 'xSendPosEmail');
        
    $xajax->processRequest();// Fonction qui va se charger de générer le Javascript, à partir des données que l'on a fournies à xAjax APRÈS AVOIR DÉCLARÉ NOS FONCTIONS.
?>
<!DOCTYPE html>
<html lang="en" ng-app>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" CONTENT="Auto-Entreprise en Meurthe-et-Moselle (Nancy). Conseil et Création de Site Web (vitrine, boutique, CMS). Hébergement, Référencement, Aide et Support seront au rendez-vous !" />
    <meta name="author" content="pancard">
    <meta name="robots" CONTENT="all" />
    <meta name="keywords" CONTENT="créateur site internet,conseils,création web,création site web,nancy,création logiciel,développement,écoute,projet,support,informatique,auto-entrepreneur,design,css3,html5,communication,tarif,54,54000,pierre-alexis nicolas,pancard,lorraine,qualité,expérience,gratuit,devis,création identité graphique,flyer,carte de visite,concept,design" />
    

    <title>Conseils et Créations Web</title>
    <link rel="shortcut icon" href="img/pancard.ico" />

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Comfortaa' rel='stylesheet' type='text/css'>

    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.3/angular.min.js"></script>
  	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.3/angular-sanitize.js"></script>  
    <script src="js/mail.js"></script>
</head>

<body>

    <!-- Side Menu -->
    <a id="menu-toggle" href="#" class="btn btn-menu btn-lg toggle"><i class="fa fa-bars"></i></a>
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="#" class="btn btn-close btn-lg pull-right toggle"><i class="fa fa-minus"></i></a>
            <li class="sidebar-brand"><a href="http://pancard.fr">Navigation</a>
            </li>
            <li>
            	<a href="#top"><i class="fa fa-home fa-2x"></i> Accueil</a>
            </li>
            <li>
		    	<a href="#about"><i class="fa fa-info-circle fa-2x"></i> A propos</a>
		    </li>
		    <li>
		    	<a href="#services"><i class="fa fa-tasks fa-2x"></i> Les Services</a>
		    </li>
		    <li>
		    	<a href="#portfolio"><i class="fa fa-flask fa-2x"></i> Les Derniers Travaux</a>
		    </li>
		    <li class="divider"></li>
		    <li>
		    	<a href="#contact"><i class="fa fa-envelope fa-2x"></i> Contact</a>
		    </li>
        </ul>
    </div>
    <!-- /Side Menu -->

    <!-- Full Page Image Header Area -->
    <div id="top" class="header">
        <div class="vert-text">
            <h1>pancard</h1>
            	<h3>
            		<em>conseils</em> & 
		        	<em>créations</em>
		        </h3>
		        <h3>
		        	<div class="btn-group">
		        		<a href="#" class="btn btn-default">
				    		<i class="fa fa-user"> Plus d'informations</i>
				    	</a>
				    	<a href="#" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
				    		<i class="caret"></i>
				    	</a>
					    <ul class="dropdown-menu">
					    	<li>
				            	<a href="#about"><i class="fa fa-info-circle"></i></a>
				            </li>
				            <li>
				            	<a href="#services"><i class="fa fa-tasks"></i></a>
				            </li>
				            <li>
				            	<a href="#portfolio"><i class="fa fa-flask"></i></a>
				            </li>
				            <li class="divider"></li>
				            <li>
				            	<a href="#contact"><i class="fa fa-envelope"></i></a>
				            </li>
					    </ul>
				    </div>
			    </h3>
			</div>
        </div>
    </div>
    <!-- /Full Page Image Header Area -->

    <!-- Intro -->
    <div id="about" class="intro">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>Depuis 2012</h2>
                    <p class="lead">
                     Créée à Nancy et récemment déménagée à Nantes, pancard est une auto-entreprise dans le conseil et la création web. <br>
                     Pierre-Alexis NICOLAS <em>{PAN}</em> - Chef de Projet Technique chez <a target="_blank" href="http://www.iadvize.com/">iAdvize</a> & Fondateur de pancard.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /Intro -->

    <!-- Services -->
    <div id="services" class="services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h2>Les Services</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-3 text-center">
                    <div class="service-item">
                        <i class="service-icon fa fa-rocket"></i>
                        <h4>Conseils</h4>
                        <p>Que ce soit technique, fonctionnelle ou sur votre activité, un avis extérieur et expérimenté permet d'avancer!</p>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="service-item">
                        <i class="service-icon fa fa-pencil"></i>
                        <h4>Créations</h4>
                        <p>Besoin d'un site web (présentation, blog, boutique en ligne...) ou d'un logiciel ? Alors nous allons nous entendre!</p>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="service-item">
                        <i class="service-icon fa fa-cloud"></i>
                        <h4>Hébergement</h4>
                        <p>Ne vous souciez plus du tout de la logistique, nous nous occupons de l'hébergement et de l'analyse statistique.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Services -->

    <!-- Callout -->
    <div class="callout">
        <div class="vert-text">
            <p>    
                <i class="fa fa-quote-left fa-1x"></i> 
                Je recommande P.A. à toute personne voulant se doter d'un nouveau site ou en désirant un plus performant.
                <br>C'est un professionnel doué, passionné et passionnant
                <i class="fa fa-quote-right fa-1x"></i>
            	<br><br>
            	<em>- Olivier - Num1nfo</em>
            </p>
        </div>
    </div>
    <!-- /Callout -->

    <!-- Portfolio -->
    <div id="portfolio" class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <h2>Les Derniers travaux</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-md-offset-2 text-center">
                    <div class="portfolio-item">
                        <a href="http://www.num1nfo.fr" target="_blank">
                            <img class="img-portfolio img-responsive" src="img/sw_num1nfo.jpg">
                        </a>
                        <h4>Site Web<br><em>Num1nfo</em></h4>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="portfolio-item">
                        <a href="#" target="_blank">
                            <img class="img-portfolio img-responsive" src="img/sm_num1nfo.jpg">
                        </a>
                        <h4>Support Marketing<br><em>Num1nfo</em></h4>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="portfolio-item">
                        <a href="http://fcrf2m.pancard.fr" target="_blank">
                            <img class="img-portfolio img-responsive" src="img/hb_fcrf2m.jpg">
                        </a>
                        <h4>Hébergement<br><em>FCRF2M</em></h4>
                    </div>
                </div>
                <div class="col-md-2 text-center">
                    <div class="portfolio-item">
                        <a href="http://janeoceramique.com" target="_blank">
                            <img class="img-portfolio img-responsive" src="img/cs_janeoceramique.jpg">
                        </a>
                        <h4>Conseils<br><em>Intégrateur de Jane'O Céreamique</em></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Portfolio -->

    <!-- Call to Action -->
    <div class="call-to-action">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h3>Vous voulez parler de votre projet ? Alors contactez-nous</h3>
                    <a href="mailto:support@pancard.fr" class="btn btn-lg btn-default">E-mail</a>
                    <a href="#contact" class="btn btn-lg btn-primary">Formulaire</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /Call to Action -->

    <!-- Map -->
    <div id="contact" class="map">
    	<!--
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
        <br />
        <small>
            <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a>
        </small>
        </iframe>
        -->
        <div class="container" ng-app="AppSendMail">
        	<div class="row">
        		<div class="col-md-4 col-md-offset-4 text-center">
                    <h2>Formulaire de contact</h2>
                    <hr>
            	</div>
            </div>
        	<div class="row" ng-controller="CtrlSendMail">
        		<div class="col-md-4">
              	<!--
              		Nom : {{nom}}&nbsp;
              			<span class="error" ng-show="contact.nom.$error.required">Obligatoire!</span>
              			<br>
              		E-mail : {{mail}}&nbsp;
              			<span class="error" ng-show="contact.mail.$error.required">Obligatoire!</span>
              			<span class="error" ng-show="contact.mail.$error.email">Adresse non valide</span>
              			<br>
              		Message : &nbsp;
              			<span class="error" ng-show="contact.message.$error.required">Obligatoire!</span>
              			<br> {{message}}
              	-->
              		<img class="img-contact img-responsive" src="img/contact.jpg">
              	</div>
        		<div class="col-md-4">
        			<form name="contact" method="post" class="form-horizontal">
                        <input type="hidden" name="nb1" ng-model="data.un"/>
                        <input type="hidden" name="nb2" ng-model="data.deux"/>
                        <div class="input-append">
              				<input type="text" name="nom" ng-model="data.nom" placeholder="Nom" class="ct-input" required/>
              				<span class="add-on fa fa-user fa-2x">&nbsp;</span>
              			</div>
              			<div class="input-append">
              				<input type="email" name="mail" ng-model="data.mail" placeholder="Adresse Electronique" class="ct-input" required/>
              				<span class="add-on fa fa-envelope fa-2x">&nbsp;</span>
              			</div>
              			<div class="input-append">
              				<textarea name="message" ng-model="data.message" placeholder="Message" class="ct-textarea" required></textarea>
              				<span class="add-on fa fa-pencil fa-2x">&nbsp;</span>
              			</div>
              			<div class="input-append">
              				<input type="text" name="secure" ng-model="data.secure" placeholder="Anti-spam : Combien font {{data.un}} + {{data.deux}}" class="ct-input" required />
              				<span ng-click="reload()" class="add-on fa fa-refresh fa-2x">&nbsp;</span>
              			</div>
              			<div class="input-append">
        					<input class="btn btn-primary" type="submit" value="Envoyer" ng-click="sendMail()"/> 
        				</div>
      				</form>
              	</div>
              	<div class="col-md-4 animated bounceInRight" id="divresultat">
              		<!--<img src="img/pancard-p.png" alt="Bien reçu !" class="img-contact-p img-responsive"/>-->
              		
              		<div class="pre">
              			<i class="fa fa-thumbs-down fa-2x" id="msg-resultat-alert"></i>
              			<i class="fa fa-thumbs-up fa-2x" id="msg-resultat-envelope"></i>
              			<span ng-model="resultat">{{resultat}}</span>
              			<br>
              			<br>
              			<span ng-model="restexte"><u>{{restexte}}</u></span>
              			<br>
              			<span ng-model="resmessage"><i>{{resmessage}}</i></span>
              			<br>
              			<br>
              			<span ng-model="reswho"><i>{{reswho}}</i></span>
              		</div>
              	</div>
              	<script type="text/javascript">
              		//angular.module('AppSendMail', ['ngSanitize'])
              		//.controller('CtrlSendMail', ['$scope', function CtrlSendMail($scope,$http) {
              		function CtrlSendMail($scope, $http) {
              			var nb1 = Math.floor(Math.random()*11);
						var nb2 = Math.floor(Math.random()*11);
					    $scope.data = {un: nb1, deux: nb2};
					    
					    $scope.eraze = function() {

					    	$('#msg-resultat-envelope').css( "display", "none" );
						    $('#msg-resultat-alert').css( "display", "none" );
					    	$('#divresultat').css( "display", "none" );
					    	$('#divresultat').css( "color", "#000000" );
					    	$scope.resultat = "";
							$scope.restexte = "";
							$scope.resmessage = "";
							$scope.reswho = "";

					    }    

					    $scope.reload = function() {
					    	var nb1 = Math.floor(Math.random()*11);
							var nb2 = Math.floor(Math.random()*11);
					    	$scope.data = {un: nb1, deux: nb2, nom: $scope.data.nom, mail: $scope.data.mail, message: $scope.data.message};
					    	$scope.eraze();
					    };

						$scope.url = 'sendmail.php';
						
					    $scope.sendMail = function() {
					    	$scope.eraze();

						    if($scope.data.nom == null || $scope.data.mail == null || $scope.data.message == null) {
						    	$('#msg-resultat-envelope').css( "display", "none" );
						    	$('#msg-resultat-alert').css( "display", "inline-block" );
						    	$scope.resultat = "Veuillez saisir les champs obligatoires";
						    }
						    else {
						    	if($scope.data.un + $scope.data.deux != $scope.data.secure) {
						    		if(typeof $scope.data.secure !== "undefined") {
						    			$scope.resultat = "Anti-spam non passé " + $scope.data.un + " + " + $scope.data.deux + " ne vaut pas " + $scope.data.secure;
						    		}
						    		else {
						    			$scope.resultat = "Veuillez saisir le contrôle anti-spam";
						    		}
						    	$('#msg-resultat-envelope').css( "display", "none" );
						    	$('#msg-resultat-alert').css( "display", "inline-block" );
						    	}
						    	else {
							        $http.post($scope.url, { "nom" : $scope.data.nom, "mail" : $scope.data.mail, "message" : $scope.data.message}).
							        success(function() {
							        	$('#msg-resultat-envelope').css( "display", "inline-block" );
						    			$('#msg-resultat-alert').css( "display", "none" );
							            $scope.resultat = "Message correctement envoyé";
							            $scope.restexte = "Rappel de votre message :";
							            $scope.resmessage = $scope.data.message;
							            $scope.reswho = $scope.data.nom + " (" + $scope.data.mail + ")";
							            $('#divresultat').css( "color", "#0099FF" );
							        })
							        .
							        error(function() {
							        	$('#msg-resultat-envelope').css( "display", "none" );
						    			$('#msg-resultat-alert').css( "display", "inline-block" );
							            $scope.resultat = "Erreur lors de l'envoi. Conseil de contact : 06 18 21 50 13";
							        });
							    }
						    };
						    $('#divresultat').css( "display", "inline-block" );
              			};
              		};
              		//]);
              		/*
              		  angular.module('AppSendMail', ['ngSanitize'])
						.controller('CtrlSendMail', ['$scope', function CtrlSendMail($scope) {
    						$scope.myHTML =
       							'I am an <code>HTML</code> <i>string</i> with <a href="#">links!</a> and other <em>stuff</em>';
  						}]);
  					*/
              	</script>
            </div>
        </div>
    </div>
    <!-- /Map -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <ul class="list-inline">
                        <li>
                        	<a href="https://www.facebook.com/pancardfr" target="_blank">
                        		<i class="fa fa-facebook fa-3x"></i>
                        	</a>
                        </li>
                        <li>
                        	<a href="https://twitter.com/pancardfr" target="_blank">
                        		<i class="fa fa-twitter fa-3x"></i>
                        	</a>
                        </li>
                        <li>
                        	<a href="https://plus.google.com/+PancardFr" target="_blank">
                        		<i class="fa fa-google-plus fa-3x"></i>
                        	</a>
                        </li>
                        <li>
                        	<a href="https://www.linkedin.com/in/pierrealexisnicolas" target="_blank">
                        		<i class="fa fa-linkedin fa-3x"></i>
                        	</a>
                        </li>
                        <li>
                        	<a href="http://fr.viadeo.com/fr/profile/pierre-alexis.nicolas" target="_blank">
                        		<i class="fa fa-viadeo fa-3x">v</i>
                        	</a>
                        </li>
                    </ul>
                    <div class="top-scroll">
                        <a href="#top"><i class="fa fa-circle-arrow-up scroll fa-4x"></i></a>
                    </div>
                    <hr>
                    <p>
                    	Copyright &copy; pancard 2012-<?php echo Date('Y'); ?>
					</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- /Footer -->

    <!-- JavaScript -->
    

    <!-- Custom JavaScript for the Side Menu and Smooth Scrolling -->
    <script>
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    </script>
    <script>
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    </script>

</body>

</html>
